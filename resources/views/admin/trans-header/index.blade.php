@extends('admin.main')
@section('title', 'Transaksi Header')
@section('content')

<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
    @include('admin.partials.validate')
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Transaksi</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>

                    <th>No Transaksi</th>
                    <th>Tanggal</th>
                    <th>Divisi</th>
                    <th>Kriteria Buah</th>
                    <th>Total Buah</th>
                    <th>Updated At</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    
                </tr>
                </thead>
                <tbody>

                @foreach($TransaksiHeader as $t)
                    <tr>
                    <td>{{$t->notrans}}</td>
                    <td>{{$t->tanggal}}</td>
                    <td>{{$t->divisi}}</td>
                    <td>{{$t->name}}</td>
                    <td>{{$t->totalbuah}}</td>
                    <td>{{$t->updated_at}}</td>
                    <td>
                      <a href="{{route('admin.trans-header.edit', $t->notrans)}}" class="btn btn-success">Edit</a>
                    </td>
                    <td>
                    
                        <form action="{{route('admin.trans-header.delete', $t->notrans)}}" method="post">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                          <button type="submit" class="btn btn-danger">Delete</a>
                        </form>
                    </td>
                   
                    </tr>
                @endforeach

                
                </tfoot>
              </table>
            </div>
          <!-- /.box -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>

@endsection
@push('style')
      <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/admin')}}/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{asset('assets/admin')}}/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/admin')}}/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endpush