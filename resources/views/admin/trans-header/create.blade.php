@extends('admin.main')
@section('title', 'Transaksi Header Create')
@section('content')

<div class="content-wrapper">


       
 
    <!-- Main content -->
    <section class="content">
      @include('admin.partials.validate')


      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Transaksi Header Create</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          
        <form action="{{route('admin.trans-header.store')}}" method="post">
        {{csrf_field()}}
        {{method_field('POST')}}
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">No Trans</label>
                  <input type="name" name="notrans" class="form-control" id="exampleInputEmail1" placeholder="Enter No Transaksi" value="{{$notrans->notrans}}">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Tanggal</label>
                  <div class="input-group date" data-provide="datepicker" data-date-format="dd-mm-yyyy">
                      <input type="name" name="tanggal" class="form-control" id="exampleInputEmail1" placeholder="Enter Tanggal">
                      <div class="input-group-addon">
                          <span class="glyphicon glyphicon-th"></span>
                      </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Divisi</label>
                  <input type="name" name="divisi" class="form-control" id="exampleInputEmail1" placeholder="Enter Divisi">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Kriteria Buah</label>
                  <select name="kriteria_buah" class="form-control selectpicker">
                    @foreach($KriteriaBuah as $c)
                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                    @endforeach
                  </select>
                  <!-- <input type="name" name="company" class="form-control" id="exampleInputEmail1" placeholder="Enter Company"> -->
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Total Buah</label>
                  <input type="name" name="totalbuah" class="form-control" id="exampleInputEmail1" placeholder="Enter Total Buah">
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
              </div>
            </form>

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>

@endsection
@push('style')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker3.min.css" >
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/locales/bootstrap-datepicker.en-GB.min.js" charset="UTF-8"></script>
<script>
$( document ).ready(function() {
  $('#datepicker').datepicker();
});
</script>
@endpush