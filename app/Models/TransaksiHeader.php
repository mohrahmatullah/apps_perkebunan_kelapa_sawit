<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransaksiHeader extends Model
{
    protected $table = 'transaksi_header';

    public function transaksidetails()
    {
        return $this->hasMany(TransaksiDetails::class);
    }
}
