<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TransaksiHeader;
use App\Models\TransaksiDetail;
use DB;

class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reportByDate()
    {
        $resultObject = DB::table('transaksi_detail as td')
        ->leftjoin('transaksi_header as th', 'th.notrans', 'td.notrans')
        ->leftjoin('kriteria_buah as kb','kb.id','td.idbuah')
        ->select('td.idbuah','td.jumlah','th.tanggal','th.divisi','kb.name')
        ->orderby('th.tanggal','ASC')
        ->get();

        $grouped = [];
        $columns = [];

        foreach ($resultObject as $row) {
            $grouped[$row->tanggal][$row->name] = $row->jumlah;
            $columns[$row->name] = $row->name;
        }
        sort($columns);
        $defaults = array_fill_keys($columns, '0');
        array_unshift($columns, 'Tanggal');
        // $arr = get_defined_vars();
        // dd($arr);

        return view('admin.reports.reports-by-date', compact('grouped','columns','defaults'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reportByDivisiDate()
    {
        $resultObject = DB::table('transaksi_detail as td')
        ->leftjoin('transaksi_header as th', 'th.notrans', 'td.notrans')
        ->leftjoin('kriteria_buah as kb','kb.id','td.idbuah')
        ->select('td.idbuah','td.jumlah','th.tanggal','th.divisi','kb.name')
        ->orderby('th.divisi','ASC')
        ->get();

        $grouped = [];
        $columns = [];

        foreach ($resultObject as $row) {
            $grouped[$row->divisi][$row->tanggal][$row->name] = $row->jumlah;
            $columns[$row->name] = $row->name;
        }
        sort($columns);
        $defaults = array_fill_keys($columns, '0');
        array_unshift($columns, 'Divisi','Tanggal');
        // $arr = get_defined_vars();
        // dd($arr);

        return view('admin.reports.reports-by-divisi-date', compact('grouped','columns','defaults'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
