<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\KriteriaBuah;
use Session;
use Auth;

class KriteriaBuahController extends Controller
{
    public function index()
    {
        $kriteria = KriteriaBuah::all();
        return view('admin.kriteria.index', compact('kriteria'));
    }

    public function create()
    {
        return view('admin.kriteria.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name'           => 'required|max:191'
        ]);

        $kriteria = new KriteriaBuah;

        $kriteria->name      = $request->name;
        $kriteria->createby     = Auth::user()->id;
        $kriteria->lastby  = Auth::user()->id;

        if($kriteria->save())
        {
            $alert_toast = 
            [
                'title' => 'Operation Successful : ',
                'text'  => 'KriteriaBuah Successfully Added.',
                'type'  => 'success',
            ];
            
        }
        else
        {
            $alert_toast = 
            [
                'title' => 'Operation Failed : ',
                'text'  => 'A Problem Occurred While Adding a KriteriaBuah.',
                'type'  => 'danger',
            ];
        }

        Session::flash('alert_toast', $alert_toast);
        return redirect()->route('admin.kriteria.index');
    }

    public function edit($id)
    {
        
        $kriteria = KriteriaBuah::findOrFail($id);
        return view('admin.kriteria.edit', compact('kriteria'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'           => 'required|max:191'
        ]);

        $kriteria = KriteriaBuah::findOrFail($id);

        $kriteria->name      = $request->name;
        $kriteria->createby     = Auth::user()->id;
        $kriteria->lastby  = Auth::user()->id;

        if($kriteria->save())
        {
            $alert_toast = 
            [
                'title' => 'Operation Successful : ',
                'text'  => 'KriteriaBuah Successfully Updated.',
                'type'  => 'success',
            ];
        }
        else
        {
            $alert_toast = 
            [
                'title' => 'Operation Failed : ',
                'text'  => 'A Problem Update The KriteriaBuah.',
                'type'  => 'danger',
            ];
        }

        Session::flash('alert_toast', $alert_toast);
        return redirect()->route('admin.kriteria.index');
    }

    public function delete(Request $request)
    {
        $kriteria = KriteriaBuah::findOrFail($request->id);
        if($kriteria->delete())
        {
            $alert_toast = 
            [
                'title' =>  'Operation Successful : ',
                'text'  =>  'KriteriaBuah Successfully Deleted.',
                'type'  =>  'success',
            ];
        }
        else
        {
            $alert_toast = 
            [
                'title' => 'Operation Failed : ',
                'text'  => 'A Problem Deleting The KriteriaBuah.',
                'type'  => 'danger',
            ];
        }

        Session::flash('alert_toast', $alert_toast);
        return redirect()->route('admin.kriteria.index');
    }
}
