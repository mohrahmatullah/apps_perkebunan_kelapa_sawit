<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TransaksiHeader;
use App\Models\TransaksiDetail;
use App\Models\KriteriaBuah;
use Session;
use Auth;

class TransHeaderController extends Controller
{
    public function index()
    {
        $TransaksiHeader = TransaksiHeader::leftjoin('transaksi_detail', 'transaksi_detail.notrans', 'transaksi_header.notrans')
        ->leftjoin('kriteria_buah','kriteria_buah.id','transaksi_detail.idbuah')
        ->orderby('transaksi_header.tanggal','DESC')->get();
        return view('admin.trans-header.index', compact('TransaksiHeader'));
    }

    public function create()
    {
        $KriteriaBuah = KriteriaBuah::all();
        $notrans = TransaksiHeader::selectRaw('LPAD(CONVERT(COUNT("id") + 1, char(8)) , 8,"0") as notrans')->first();
        return view('admin.trans-header.create', compact('KriteriaBuah','notrans'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'notrans'          => 'required|unique:transaksi_header',
            'tanggal'           => 'required',
            'divisi'           => 'required|numeric',
            'totalbuah'           => 'required|numeric'
        ]);

        $TransaksiHeader = new TransaksiHeader;

        $TransaksiHeader->notrans      = $request->notrans;
        $TransaksiHeader->tanggal     = $request->tanggal;
        $TransaksiHeader->divisi  = $request->divisi;
        $TransaksiHeader->totalbuah  = $request->totalbuah;
        $TransaksiHeader->createby     = Auth::user()->id;
        $TransaksiHeader->lastby  = Auth::user()->id;

        if($TransaksiHeader->save())
        {
            $TransaksiDetail = new TransaksiDetail;
            $TransaksiDetail->notrans      = $request->notrans;
            $TransaksiDetail->idbuah     = $request->kriteria_buah;
            $TransaksiDetail->jumlah  = $request->totalbuah;
            $TransaksiDetail->createby     = Auth::user()->id;
            $TransaksiDetail->lastby  = Auth::user()->id;

            $TransaksiDetail->save();

            $alert_toast = 
            [
                'title' => 'Operation Successful : ',
                'text'  => 'Transaksi Header Successfully Added.',
                'type'  => 'success',
            ];
            
        }
        else
        {
            $alert_toast = 
            [
                'title' => 'Operation Failed : ',
                'text'  => 'A Problem Occurred While Adding a Transaksi Header.',
                'type'  => 'danger',
            ];
        }



        Session::flash('alert_toast', $alert_toast);
        return redirect()->route('admin.trans-header.index');
    }

    public function edit($id)
    {
        
        $trans = TransaksiHeader::where('transaksi_header.notrans',$id)
        ->leftjoin('transaksi_detail','transaksi_detail.notrans','transaksi_header.notrans')
        ->select('transaksi_header.notrans','transaksi_header.tanggal','transaksi_header.divisi','transaksi_header.totalbuah','transaksi_detail.idbuah')
        ->first();
        $KriteriaBuah = KriteriaBuah::all();
        return view('admin.trans-header.edit', compact('trans','KriteriaBuah'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'tanggal'           => 'required',
            'divisi'           => 'required|numeric',
            'totalbuah'           => 'required|numeric'
        ]);
        $TransaksiHeader = array(
                'tanggal'                  => $request->tanggal,
                'divisi'             => $request->divisi,
                'totalbuah'             => $request->totalbuah,
                'createby'               => Auth::user()->id,
                'lastby'               => Auth::user()->id,
                'updated_at'            => date("y-m-d H:i:s", strtotime('now'))
              );

        if(TransaksiHeader::where('notrans', $id)->update($TransaksiHeader))
        {
            $TransaksiDetail = array(
                'idbuah'                  => $request->kriteria_buah,
                'jumlah'             => $request->totalbuah,
                'createby'               => Auth::user()->id,
                'lastby'               => Auth::user()->id,
                'updated_at'            => date("y-m-d H:i:s", strtotime('now'))
              );
            TransaksiDetail::where('notrans', $id)->update($TransaksiDetail);

            $alert_toast = 
            [
                'title' => 'Operation Successful : ',
                'text'  => 'TransaksiHeader Successfully Updated.',
                'type'  => 'success',
            ];
        }
        else
        {
            $alert_toast = 
            [
                'title' => 'Operation Failed : ',
                'text'  => 'A Problem Update The TransaksiHeader.',
                'type'  => 'danger',
            ];
        }

        Session::flash('alert_toast', $alert_toast);
        return redirect()->route('admin.trans-header.index');
    }

    public function delete(Request $request)
    {
        if(TransaksiHeader::where('notrans',$request->id)->delete() && TransaksiDetail::where('notrans',$request->id)->delete())
        {
            $alert_toast = 
            [
                'title' =>  'Operation Successful : ',
                'text'  =>  'TransaksiHeader Successfully Deleted.',
                'type'  =>  'success',
            ];
        }
        else
        {
            $alert_toast = 
            [
                'title' => 'Operation Failed : ',
                'text'  => 'A Problem Deleting The TransaksiHeader.',
                'type'  => 'danger',
            ];
        }

        Session::flash('alert_toast', $alert_toast);
        return redirect()->route('admin.trans-header.index');
    }
}
