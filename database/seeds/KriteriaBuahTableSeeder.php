<?php

use Illuminate\Database\Seeder;

class KriteriaBuahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kriteria_buah')->insert([
            [
                'name'     => 'Matang',
                'createby' => 1,
                'lastby' => 1
            ]
        ]);
    }
}
