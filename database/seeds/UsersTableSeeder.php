<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name'     => 'Rahmat',
                'email'    => 'admin@example.com',
                'password' => bcrypt('password'),
                'createby' => 1,
                'lastby' => 1
            ]
        ]);
    }
}
