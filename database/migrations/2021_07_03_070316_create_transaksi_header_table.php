<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_header', function (Blueprint $table) {
            $table->string('notrans',20);
            $table->string('tanggal',20);
            $table->integer('divisi');
            $table->integer('totalbuah');
            $table->string('createby',20);
            $table->string('lastby',20);
            $table->primary('notrans');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_header');
    }
}
