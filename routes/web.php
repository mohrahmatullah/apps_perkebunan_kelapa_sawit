<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * @author Batuhan Batal <batuhanbatal@gmail.com>
 */

Route::get('/', function () {
    return redirect()->route('auth.login');
});


// Admin Routes
Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin', 'middleware' => 'auth'], function(){
    // Index   
    Route::get('/','HomeController@index')->name('.index');
    // User Index
    Route::get('/users','UsersController@index')->name('.users.index');
    // User Create 
    Route::get('/users/create','UsersController@create')->name('.users.create');
    Route::post('/users/store','UsersController@store')->name('.users.store');
    // User Edit
    Route::get('/users/edit/{id}','UsersController@edit')->name('.users.edit');
    Route::put('/users/edit/{id}','UsersController@update')->name('.users.update');
    // User Delete
    Route::delete('/users/delete/{id}','UsersController@delete')->name('.users.delete');

    Route::get('/kriteria','KriteriaBuahController@index')->name('.kriteria.index');
    // kriteria Create 
    Route::get('/kriteria/create','KriteriaBuahController@create')->name('.kriteria.create');
    
    // kriteria Edit
    Route::get('/kriteria/edit/{id}','KriteriaBuahController@edit')->name('.kriteria.edit');
    Route::put('/kriteria/update/{id}','KriteriaBuahController@update')->name('.kriteria.update');
    // kriteria Delete
    Route::delete('/kriteria/delete/{id}','KriteriaBuahController@delete')->name('.kriteria.delete');

    Route::get('/trans-header','TransHeaderController@index')->name('.trans-header.index');
    // trans-header Create 
    Route::get('/trans-header/create','TransHeaderController@create')->name('.trans-header.create');
    Route::post('/kriteria/store','TransHeaderController@store')->name('.trans-header.store');
    // trans-header Edit
    Route::get('/trans-header/edit/{id}','TransHeaderController@edit')->name('.trans-header.edit');
    Route::put('/trans-header/update/{id}','TransHeaderController@update')->name('.trans-header.update');
    // trans-header Delete
    Route::delete('/trans-header/delete/{id}','TransHeaderController@delete')->name('.trans-header.delete');

    Route::get('/reports-by-date','ReportsController@reportByDate')->name('.reports-by-date');
    Route::get('/reports-by-divisi-date','ReportsController@reportByDivisiDate')->name('.reports-by-divisi-date');

});

Route::post('/kriteria/store','Admin\KriteriaBuahController@store')->name('ft');

// Auth Routes
Route::group(['namespace' => 'Auth', 'as' => 'auth'], function(){
    // Login Page 
    Route::get('/login','LoginController@showLoginForm')->name('.login');
    // Login Post
    Route::post('/login','LoginController@login')->name('.login');
    // Logout
    Route::get('/logout','LoginController@logout')->name('.logout');
});