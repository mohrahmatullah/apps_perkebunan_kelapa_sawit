## Project to manage company with their employees
Please make sure it is connected to the internet

### Project install
		
		git clone https://gitlab.com/mohrahmatullah/apps_perkebunan_kelapa_sawit
		

### Project key generate
		
		php artisan key:generate
		
### Database connection
Create database and create find these fields in the .env file and enter your information

		
		DB_DATABASE=
		DB_USERNAME=
		DB_PASSWORD=
		

### Cache clear
		
		php artisan config:cache
		
### If Use Docker
		
		docker exec -it container_id bash

### Make migrate
		
		php artisan migrate
		

### Make seed
		
		php artisan db:seed
		

### Or export database on directory
		
		sql/apps_perkebunan_kelapa_sawit.sql
		

### If use linux

		php artisan route:clear
		php artisan config:clear
		php artisan cache:clear
		chmod -R 777 storage
		chmod -R 777 bootstrap/cache

### Run project
		
		php artisan serve
		  
### Login info for admin
		
		email    : admin@example.com
		password : password
		