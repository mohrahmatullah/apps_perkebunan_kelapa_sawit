-- Adminer 4.8.1 MySQL 8.0.25 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `kriteria_buah`;
CREATE TABLE `kriteria_buah` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `kriteria_buah` (`id`, `name`, `createby`, `lastby`, `created_at`, `updated_at`) VALUES
(1,	'Matang',	'1',	'1',	NULL,	NULL),
(2,	'Lewat Matang',	'1',	'1',	'2021-07-04 01:09:35',	'2021-07-04 01:09:35'),
(3,	'Busuk',	'1',	'1',	'2021-07-04 01:09:51',	'2021-07-04 01:09:51'),
(4,	'Tangkai Panjang',	'1',	'1',	'2021-07-04 01:10:02',	'2021-07-04 01:10:02');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2021_07_03_065310_create_kriteria_buah_table',	1),
(3,	'2021_07_03_070316_create_transaksi_header_table',	1),
(4,	'2021_07_03_070323_create_transaksi_detail_table',	1);

DROP TABLE IF EXISTS `transaksi_detail`;
CREATE TABLE `transaksi_detail` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `notrans` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idbuah` int NOT NULL,
  `jumlah` int NOT NULL,
  `createby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `transaksi_header`;
CREATE TABLE `transaksi_header` (
  `notrans` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `divisi` int NOT NULL,
  `totalbuah` int NOT NULL,
  `createby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`notrans`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `createby`, `lastby`, `remember_token`, `created_at`, `updated_at`) VALUES
(1,	'Rahmat',	'admin@example.com',	'$2y$10$1EkfxwMzJWAxNCAmSbEmuOw5t84HsNqFVz/KkndklMuy2Y.OMAOk6',	'1',	'1',	NULL,	NULL,	NULL);

-- 2021-07-04 01:10:52
